class UCDLookup

  require 'csv'
  require 'net/http'
  require 'json'

  BASE_URL = "https://iet-ws.ucdavis.edu/api/iam/people"

  # grab data from IAMS
  def self.get_iam_id_from_kerberos(kerb)
    id_base_url = "#{BASE_URL}/prikerbacct/search?v=1.0&key=#{ENV['IAM_KEY']}"
    (UCDLookup.get_json("#{id_base_url}&userId=#{kerb}") || Hash.new)["iamId"]
  end

  def self.get_json(url)
    response = Net::HTTP.get(URI(url))
    results = JSON.parse(response)["responseData"]["results"]&.first
  end

  def self.get_iam_id_from_email(email)
    id_base_url = "#{BASE_URL}/contactinfo/search?v=1.0&key=#{ENV['IAM_KEY']}"
    (UCDLookup.get_json("#{id_base_url}&email=#{email}") || Hash.new)["iamId"]
  end

  def self.get_iam_id_from_name(last, first, source = "o")
    id_base_url = "#{BASE_URL}/search?v=1.0&key=#{ENV['IAM_KEY']}"
    (UCDLookup.get_json("#{id_base_url}&#{source}FirstName=#{first}&#{source}LastName=#{last}") || Hash.new)["iamId"]
  end

  def self.get_details_from_iam(iam_id)
    personal_base_url = "#{BASE_URL}/#{iam_id}?v=1.0&key=#{ENV['IAM_KEY']}"
    personal_info = UCDLookup.get_json(personal_base_url)

    login_base_url = "#{BASE_URL}/prikerbacct/#{iam_id}?v=1.0&key=#{ENV['IAM_KEY']}"
    login_info = UCDLookup.get_json(login_base_url)

    contact_base_url = "#{BASE_URL}/contactinfo/#{iam_id}?v=1.0&key=#{ENV['IAM_KEY']}"
    contact_info = UCDLookup.get_json(contact_base_url)

    return {firstname: personal_info["dFirstName"], lastname: personal_info["dLastName"],
            email: contact_info["email"], workphone: contact_info["workPhone"], workcell: contact_info["workCell"], kerberos: login_info["userId"], studentID: personal_info["studentId"],
            isEmployee: personal_info["isEmployee"], isFaculty: personal_info["isFaculty"], isStudent: personal_info["isStudent"],
            isStaff: personal_info["isStaff"] }
  end

  def self.get_details_from_kerberos(kerb)
    iam_id = self.get_iam_id_from_kerberos(kerb)
    return false if iam_id.nil?

    return(UCDLookup.get_details_from_iam(iam_id))
  end

  def self.get_details_from_email(email)
    iam_id = self.get_iam_id_from_email(email)
    return false if iam_id.nil?

    return(UCDLookup.get_details_from_iam(iam_id))
  end

  def self.get_details_from_name(last, first, source = "o")
    iam_id = self.get_iam_id_from_name(last, first, source)
    return false if iam_id.nil?

    return(UCDLookup.get_details_from_iam(iam_id))
  end

  def self.lookup(identifier)
    # check if identifier is an email address
    return(UCDLookup.get_details_from_email(identifier)) if identifier =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    return(UCDLookup.get_details_from_kerberos(identifier))
  end

  # grab data from CSV dumped from banana sql

  def read_from_file
    users = Hash.new
    CSV.foreach('data/all_ps_users.txt', :headers => true, :col_sep => ",") do |row|
      entry = row.to_hash
      users[entry['kerberos_id']] = entry
    end
    return users
  end

  def get_user_by_kerberos(kerberos)
    users = read_from_file
    user = users[kerberos]
    if user.present? then
      user['last_name'] = user['last_name'].downcase.titleize
      user['first_name'] = user['first_name'].downcase.titleize
    end
    user
  end

  def get_role_by_category(cat)
    # Student", "Staff", "Emeritus", "Other Academic", "Faculty"
    role = nil
    case cat
    when "Student"
      role = Role.find_by(name: "Graduate")
    when "Staff"
      role = Role.find_by(name: "Staff")
    when "Faculty"
      role = Role.find_by(name: "Faculty CE")
      # FIXME BIG TIME
    end
    return role
  end

  # def create_new_user_by_kerberos(kerberos)
  #   user_details = get_user_by_kerberos(kerberos)
  #   if user_details.present? then
  #     # figure out what role the new user should be
  #     role = get_role_by_category(user_details['cat'])
  #     if role.present? then
  #       # create the user
  #       return user
  #     else
  #       return nil
  #   else
  #     return nil
  # end

  # def self.get_field_from_directory(field, match)
  #   mech = Mechanize.new
  #   begin
  #     # mech.get "http://email.ucdavis.edu/cgi-bin/mailid_n.pl?mailid=#{field}" do |page|
  #     mech.get "https://iet.ucdavis.edu/ucd/email_id_detective/#{field}" do |page|
  #     return page.search(match).first.next.text
  #     end
  #   rescue Exception
  #     return "An error occured which prevented results."
  #   end
  # end
  #
  # def self.get_kerberos_from_email(email)
  #   get_field_from_directory(email, "td:contains('LoginID (username)')")
  # end
  #
  # def self.get_email_from_kerberos(kerberos)
  #   get_field_from_directory(kerberos, "td:contains('Email address:')")
  # end
  #
  # def self.get_whois_information(kerberos)
  #   mech = Mechanize.new
  #   user_details = Hash.new
  #   begin
  #     mech.get "http://email.ucdavis.edu/cgi-bin/mailid_n.pl?mailid=#{kerberos}" do |page|
  #     #mech.get "http://directory.ucdavis.edu/search/directory_results.shtml?filter=#{self.kerberos}" do |page|
  #       email = /(\w+)@ucdavis.edu/.match(page.search("td").text).to_s
  #       if !email.blank? then
  #
  #         mech.get "http://directory.ucdavis.edu/search/directory_results.shtml?filter=#{email.split('@').first}" do |nextpage|
  #
  #           name = nextpage.search('tr').at("th:contains('Name')").parent.search('td').first.try('text')
  #           user_details[:name] = name
  #           user_details[:title] = nextpage.search('tr').at("th:contains('Title')").parent.search('td').first.try('text')
  #           user_details[:phone] = nextpage.search('tr').at("th:contains('Phone')").parent.search('td').first.try('text')
  #           user_details[:department] = nextpage.search('tr').at("th:contains('Department')").parent.search('td').first.try('text')
  #
  #           user_details[:firstname] = name.split.first if !name.nil?
  #           user_details[:lastname] = name.split.last if !name.nil?
  #
  #           return user_details
  #
  #         end
  #       end
  #     end
  #   rescue Exception
  #     # session[:cas_user] = nil
  #     # redirect_to login_path, notice: "There appears to be a problem accessing your user details. Please try again."
  #     return
  #   end
  # end

end
