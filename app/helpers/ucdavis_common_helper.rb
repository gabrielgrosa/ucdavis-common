module UcdavisCommonHelper

  def menu_item(label, &blk)
    content_tag :li, class: :dropdown do
      concat(link_to('#', class: "dropdown-toggle", "data-toggle" => "dropdown") do
        concat(label)
        concat(content_tag(:span, class: :caret) do
        end) # caret
      end) # link
      concat(content_tag(:ul, class: "dropdown-menu") do
        concat capture(&blk)
      end) # ul
    end # li
  end # def

  def modal_button(id, title, &body)
    content_tag(:span, class: "btn btn-primary", "data-toggle" => "modal", "data-target" => "##{id}") do
      concat title
    end
  end

  def modal_body(id, title, &body)
    content_tag(:div, id: id, class: "modal fade", tabindex: "-1", role: 'dialog', "aria-labelledby" => "newCropModalLabel", "aria-hidden" => "true") do
      concat(content_tag(:div, class: "modal-dialog") do
        concat(content_tag(:div, class: "modal-content") do
          concat(content_tag(:div, class: "modal-header") do
            concat(content_tag(:button, class: :close, "data-dismiss" => "modal", "data-target" => id) do
              concat(content_tag(:span, "aria-hidden" => "true") do
                concat raw('&times;')
              end)
              concat(content_tag(:span, class: "sr-only") do
                concat "Close"
              end)
            end) # button
            concat(content_tag(:h4, id: "#{id}Label", class: "modal-title") do
              concat title
            end)
          end) # modal header
          concat(content_tag(:div, class: "modal-body") do
            concat capture(&body)
          end) # modal body
        end) # modal content
      end) # modal dialog
    end # modal fade
  end # def

  def link_to_add_fields(name, f, association, prefix = "")
      new_object = f.object.send(association).klass.new
      id = new_object.object_id
      fields = f.fields_for(association, new_object, child_index: id) do |builder|
        render(prefix + association.to_s.singularize + "_fields", f: builder)
      end
      link_to(name, '#', class: "add_fields btn btn-primary", style: "font-size:x-large;", data: {id: id, fields: fields.gsub("\n", "")})
    end
end
