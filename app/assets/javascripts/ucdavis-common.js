//= require ucdavis-common/accordion
//= require ucdavis-common/datepickers
//= require ucdavis-common/dynamic_forms
//= require ucdavis-common/lazy_tables
//= require ucdavis-common/notices
//= require jquery-tablesorter/addons/pager/jquery.tablesorter.pager
//= require ucdavis-common/pager
//= require ucdavis-common/spinner
//= require ucdavis-common/tables
