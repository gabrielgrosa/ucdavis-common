jQuery ->		
	$(".dynamic_table").tablesorter
		sortList: [[0,0]]
		widgets: ['filter']
		ignoreCase: true
		widgetOptions:
			filter_columnFilters : true
			filter_hideEmpty: true

jQuery ->
	$(".dynamic_table_hide_filter").tablesorter
		sortList: [[0,0]]
		widgets: ['filter']
		ignoreCase: true
		widgetOptions:
			filter_columnFilters : true
			filter_hideEmpty: true
			filter_hideFilters: true
