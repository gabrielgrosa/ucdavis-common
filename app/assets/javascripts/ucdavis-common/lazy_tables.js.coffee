root = exports ? this

root.update_table = (element) ->
  obj = $('#' + element)
  if obj.find('tbody').html().length == 0
    requestAnimationFrame ->
      update_table element
      return
  else
    obj.trigger('updateAll')
    obj.trigger('search', false)
  return

jQuery ->
  $('.lazy_accordion').accordion
    header: '.accordion-head'
    heightStyle: 'content'
    collapsible: true
    animate: false
    beforeActivate: (event, ui) ->
      if ui.newPanel.find('tbody').html() == ''
        ui.newPanel.find('tbody').load ui.newPanel.data('url')
      return
    activate: (event, ui) ->
      update_table(ui.newPanel.find('table').attr('id'))
      return

jQuery ->
  $('.lazy_accordion_closed').accordion
    header: '.accordion-head'
    heightStyle: 'content'
    collapsible: true
    active: false
    animate: false
    beforeActivate: (event, ui) ->
      if ui.newPanel.find('tbody').html() == ''
        ui.newPanel.find('tbody').load ui.newPanel.data('url')
      return
    activate: (event, ui) ->
      update_table(ui.newPanel.find('table').attr('id'))
      return
